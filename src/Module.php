<?php

namespace ticmakers\generators;

/**
* Inicializador de módulo.
*
* @package ticmakers
* @subpackage generators
* @category Module Config
*
* @property string $controllerNamespace Namespace de los controladores
*
* @author Kevin Daniel Guzmán Delgadillo <kevindanielguzmen98@gmail.com>
* @copyright Copyright (c) 2018 Tic Makers S.A.S.
* @version 0.0.1
* @since 2.0.0
*/
class Module extends \yii\gii\Module
{
    public $controllerNamespace = 'ticmakers\generators\controllers';

    /**
     * Retorna la lista de los generadores disponibles.
     *
     * @return array
     */
    protected function coreGenerators()
    {
        return [
            'model' => ['class' => 'ticmakers\generators\core\model\Generator'],
            'crud' => ['class' => 'ticmakers\generators\core\crud\Generator'],
            'rest' => ['class' => 'ticmakers\generators\core\rest\Generator']
        ];
    }
}