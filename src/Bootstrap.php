<?php

namespace ticmakers\generators;

use Yii;
use yii\base\Application;
use yii\base\BootstrapInterface;

/**
 * Class Bootstrap
 *
 * @package ticmakers\yii2-generators
 * @author Juan Sebastian Muñoz Reyes <juan.munoz@ticmakers.com>
 */
class Bootstrap implements BootstrapInterface
{
    /**
     * Bootstrap method to be called during application bootstrap stage.
     *
     * @param Application $app the application currently running
     */
    public function bootstrap($app)
    {
        if ($app->hasModule('gii')) {
            if (!isset($app->getModule('gii')->generators['rest'])) {
                $app->getModule('gii')
                    ->generators['rest'] = [
                    'class' => 'ticmakers\generators\core\rest\Generator',
                ];
            }
            if (!isset($app->getModule('gii')->generators['redis'])) {
                $app->getModule('gii')
                    ->generators['redis'] = [
                    'class' => 'ticmakers\generators\core\redis\Generator',
                ];
            }
            if (!isset($app->getModule('gii')->generators['tic-crud'])) {
                $app->getModule('gii')
                    ->generators['tic-crud'] = [
                    'class' => 'ticmakers\generators\core\crud\Generator',
                ];

                unset($app->getModule('gii')->generators['crud']);
            }
            if (!isset($app->getModule('gii')->generators['tic-model'])) {
                $app->getModule('gii')
                    ->generators['tic-model'] = [
                    'class' => 'ticmakers\generators\core\model\Generator',
                ];
                unset($app->getModule('gii')->generators['model']);
            }
            if (!isset($app->getModule('gii')->generators['migrations'])) {
                $app->getModule('gii')
                    ->generators['migrations'] = [
                    'class' => 'ticmakers\generators\core\migration\Generator',
                ];
            }
        }
    }
}
