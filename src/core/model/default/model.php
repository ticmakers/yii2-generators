<?php

/**
 * This is the template for generating the model class of a specified table.
 */

/* @var $this yii\web\View */
/* @var $generator yii\gii\generators\model\Generator */
/* @var $tableName string full table name */
/* @var $className string class name */
/* @var $queryClassName string query class name */
/* @var $tableSchema yii\db\TableSchema */
/* @var $labels string[] list of attribute labels (name => label) */
/* @var $rules string[] list of validation rules */
/* @var $relations array list of relations (name => relation declaration) */

echo "<?php\n";
?>

namespace <?= $generator->ns ?>;

use Yii;

/**
 * Éste es el modelo para la tabla "<?= $generator->generateTableName($tableName) ?>".
 * <?= $generator->getComment($tableName) . "\n" ?>
 *
 * @package app
 * @subpackage models/base
 * @category models
 *
<?php foreach ($tableSchema->columns as $column) : ?>
 * @property <?= "{$column->phpType} \${$column->name} " . str_replace(["\n", "\r"], [" ", " "], $column->comment) . "\n" ?>
<?php endforeach; ?>
<?php if (!empty($relations)) : ?>
<?php foreach ($relations as $name => $relation) : ?>
 * @property <?= $relation[1] . ($relation[2] ? '[]' : '') . ' $' . lcfirst($name) . " Datos relacionados con modelo \"{$relation[1]}\"\n" ?>
<?php endforeach; ?>
<?php endif; ?>
 *
 * @author <?= $generator->getDevelopers($generator->developer) ?> <<?= $generator->developer ?>>
 * @copyright Copyright (c) <?= date('Y') . " {$generator->copyright}\n" ?>
 * @version 0.0.1
 * @since <?= $generator->since . "\n" ?>
 */
class <?= $className ?> extends <?= '\\' . ltrim($generator->baseClass, '\\') . "\n" ?>
{
    /**
     * Definición del nombre de la tabla.
     *
     * @return string
     */
    public static function tableName()
    {
        return '<?= $generator->generateTableName($tableName) ?>';
    }
<?php if ($generator->db !== 'db') : ?>

    /**
     * Crear la conexión a la BD.
     *
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('<?= $generator->db ?>');
    }
<?php endif; ?>

    /**
     * Define las reglas, filtros y datos por defecto para las columnas.
     *
     * @return array
     */
    public function rules()
    {
        return [<?= "\n            " . implode(",\n            ", $rules) . ",\n        " ?>];
    }

    /**
     * Define los labels para las columnas del modelo.
     *
     * @return array
     */
    public function attributeLabels()
    {
        return [
<?php foreach ($labels as $name => $label) : ?>
            <?= "'$name' => " . $generator->generateString($label) . ",\n" ?>
<?php endforeach; ?>
        ];
    }

    /**
     * Retorna los textos de las ayudas de los campos.
     *
     * @return array|string
     */
    public function getHelp($attribute = null)
    {
        $helps = [
<?php foreach ($tableSchema->columns as $column) : ?>
            <?= "'{$column->name}' => " . $generator->generateString(str_replace(["\n", "\r"], [" ", " "], $column->comment)) . ",\n" ?>
<?php endforeach; ?>
        ];
        return !is_null($attribute) && isset($helps[$attribute]) ? $helps[$attribute] : $helps;
    }

<?php foreach ($relations as $name => $relation) : ?>
    /**
     * Definición de la relación con el modelo "<?= $name ?>".
     *
     * @return <?= "\\" . $generator->appNs . "\\" . $name . "\n" ?>
     */
    public function get<?= $name ?>()
    {
        <?= $relation[0] . "\n" ?>
    }

<?php endforeach; ?>
<?php if ($queryClassName) : ?>
<?php
$queryClassFullName = ($generator->ns === $generator->queryNs) ? $queryClassName : '\\' . $generator->queryNs . '\\' . $queryClassName;
echo "\n";
?>
    /**
     * Retorna una instacia de la clase actual.
     *
     * @return <?= $queryClassFullName ?> the active query used by this AR class.
     */
    public static function find()
    {
        return new <?= $queryClassFullName ?>(get_called_class());
    }
<?php endif; ?>
    /**
     * Retorna un arreglo tipo clave valor según la llave primaria y la primera columna no primaria de la tabla.
     *
     * @param boolean $list Si necesitas un List (por lo general para dropdownlist) o no
     * @param array $attributes Condiciones para la consulta
     * @return array|<?= $className . "[]\n" ?>
     */
    public static function getData($list = true, $attributes = [], $orderBy = [])
    {
        if (!isset($attributes[static::STATUS_COLUMN])) {
            $attributes[static::STATUS_COLUMN] = static::STATUS_ACTIVE;
        }

        if(empty($orderBy)){
            $orderBy[static::getNameFromRelations()] = SORT_ASC;
        }

        $query = new \ArrayObject(static::find()->where($attributes)->orderBy($orderBy)->cache(3)->asArray()->all());
        if ($list) {
            $query = \yii\helpers\ArrayHelper::map($query, '<?= is_array($tableSchema->primaryKey) ? $tableSchema->primaryKey[0] : $tableSchema->primaryKey; ?>', static::getNameFromRelations());
        }
        return $query;
    }

}