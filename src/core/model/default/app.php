<?php

/**
 * This is the template for generating the model class of a specified table.
 */

/* @var $this yii\web\View */
/* @var $generator yii\gii\generators\model\Generator */
/* @var $tableName string full table name */
/* @var $className string class name */
/* @var $queryClassName string query class name */
/* @var $tableSchema yii\db\TableSchema */
/* @var $labels string[] list of attribute labels (name => label) */
/* @var $rules string[] list of validation rules */
/* @var $relations array list of relations (name => relation declaration) */

echo "<?php\n";
?>

namespace <?= $generator->appNs ?>;

use Yii;

/**
 * Éste es el modelo para la tabla "<?= $generator->generateTableName($tableName) ?>".
 * <?= $generator->getComment($tableName) . "\n" ?>
 *
 * @package <?= $generator->appNs ?> 
 *
<?php foreach ($tableSchema->columns as $column) : ?>
 * @property <?= "{$column->phpType} \${$column->name} " . str_replace(["\n", "\r"], [" ", " "], $column->comment) . "\n" ?>
<?php endforeach; ?>
<?php if (!empty($relations)) : ?>
<?php foreach ($relations as $name => $relation) : ?>
 * @property <?= $relation[1] . ($relation[2] ? '[]' : '') . ' $' . lcfirst($name) . " Datos relacionados con modelo \"{$relation[1]}\"\n" ?>
<?php endforeach; ?>
<?php endif; ?>
 *
 * @author <?= $generator->getDevelopers($generator->developer) ?> <<?= $generator->developer ?>>
 * @copyright Copyright (c) <?= date('Y') . " {$generator->copyright}\n" ?>
 */
class <?= $className ?> extends <?= '\\' . ltrim($generator->ns, '\\') . '\\' . $className . "\n" ?>
{
    /**
     * @inheritDoc
     */
    public function rules()
    {
        $rules = [];
        return Yii::$app->arrayHelper::merge(parent::rules(), $rules);
    }

    /**
     * @inheritDoc
     */
    public function attributeLabels()
    {
        $attributeLabels = [];
        return Yii::$app->arrayHelper::merge(parent::attributeLabels(), $attributeLabels);
    }

    /**
     * @inheritDoc
     */
    public function getHelp($attribute = null)
    {
        $newhelps = [];
        $helps = Yii::$app->arrayHelper::merge(parent::getHelp(), $newhelps);
        return !is_null($attribute) && isset($helps[$attribute]) ? $helps[$attribute] : $helps;
    }

    /**
     * @inheritDoc
     */
    public static function getNameFromRelations()
    {
        return parent::getNameFromRelations();
    }

    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        $newBehaviors = [];
        return Yii::$app->arrayHelper::merge(parent::behaviors(), $newBehaviors);
    }

    /**
     * @inheritDoc
     */
    public function formColumns()
    {
        $formColumns = [];
        return Yii::$app->arrayHelper::merge(parent::formColumns(), $formColumns);
    }

    /**
     * @inheritDoc
     */
    public function gridColumns()
    {
        $gridColumns = [];
        return Yii::$app->arrayHelper::merge(parent::gridColumns(), $gridColumns);
    }

}
