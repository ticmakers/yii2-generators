<?php

/**
 * This is the template for generating the model class of a specified table.
 */

/* @var $this yii\web\View */
/* @var $generator mootensai\enhancedgii\migration\Generator */
/* @var $migrationName string migration name */

echo "<?php

namespace app\migrations;

use Yii;
use yii\db\Schema;

class {$migrationName} extends \yii\db\Migration
{
    public \$tableName = '{$tableName}';
";

if ($generator->isSafeUpDown) :
    echo "    public function safeUp()";
else :
    echo "    public function up()";
endif;
echo "{\n";
if ($generator->createTableIfNotExists) :
    echo "        \$tables = Yii::\$app->db->schema->getTableNames();";
endif;

echo "        \$tableOptions = null;
        if (\$this->db->driverName === 'mysql') {
            \$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }\n";
?>
<?php foreach ($tables as $table) :

    $tableRaw = trim($table['name'], '{}%');
    $t = '';
    if ($generator->createTableIfNotExists == 1) : $t = '  ';
        ?>
        if (!in_array(Yii::$app->db->tablePrefix.'<?= $tableRaw ?>', $tables)) {
    <?php endif; ?>
    <?= $t ?>
    $this->createTable('<?= $tableRaw ?>', [
    <?php foreach ($table['columns'] as $column => $definition) : ?>
        <?= $t ?>
        <?= "'$column' => $definition" ?>,
    <?php endforeach; ?>
    <?php if (isset($table['primary'])) : ?>
        <?= $t ?>
        <?= "'{$table['primary']}'" ?>,
    <?php endif; ?>
    <?php foreach ($table['relations'] as $definition) : ?>
        <?= $t ?>
        <?= "'$definition'" ?>,
    <?php endforeach; ?>
    <?= $t ?>], $tableOptions);


    <?php foreach ($table['checks'] as $check) : ?>
        $this->execute("ALTER TABLE <?= $tableRaw ?> ADD CONSTRAINT \"<?= $check->name ?>\"
        CHECK <?= $check->expression ?>");
    <?php endforeach; ?>

    <?php foreach ($table['tableSchema']->columns as $column => $columnData) : ?>
        $this->addCommentOnColumn('<?= $tableRaw ?>', '<?= $column ?>', "<?= $columnData->comment ?>");
    <?php endforeach; ?>

    <?php if ($generator->createTableIfNotExists == 1) : ?>
        } else {
        echo "\nTable `".Yii::$app->db->tablePrefix."<?= $tableRaw ?>` already exists!\n";
        }
    <?php endif; ?>
<?php endforeach; ?>


}

<?php if ($generator->isSafeUpDown) : ?>
    public function safeDown()
<?php else : ?>
    public function down()
<?php endif; ?>
{
<?php if ($generator->disableFkc) : ?>
    $this->execute('SET foreign_key_checks = 0');
<?php endif; ?>
<?php foreach (array_reverse($tables) as $table) : ?>
    $this->dropTable('<?= trim($table['name'], '{}%') ?>');
<?php endforeach; ?>
<?php if ($generator->disableFkc) : ?>
    $this->execute('SET foreign_key_checks = 1');
<?php endif; ?>
}
}