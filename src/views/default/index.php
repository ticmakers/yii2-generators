
<?php
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $generators \yii\gii\Generator[] */
/* @var $content string */

$generators = Yii::$app->controller->module->generators;
$this->title = Yii::t('app', 'Generador de TIC Makers');
?>
<div class="default-index">
    <h1 class="border-bottom pb-3 mb-3"><?= $this->title ?><small> una herramienta mágica para generar código.</small></h1>
    <p class="lead mb-5">Empecemos seleccionado un generador:</p>
    <div class="row">
        <?php foreach ($generators as $id => $generator): ?>
        <div class="generator col-lg-4">
            <h3><?= Html::encode($generator->getName()) ?></h3>
            <p><?= $generator->getDescription() ?></p>
            <p><?= Html::a('Empezar &raquo;', ['default/view', 'id' => $id], ['class' => ['btn', 'btn-outline-secondary']]) ?></p>
        </div>
        <?php endforeach; ?>
    </div>
</div>
